<?php
/***********************************************************************************************
Usage Example 
Just include this file at the top, where you want to manage the session using session data


To set session data in DB 
Case 1: Set a single session data, pass the key and value in array
$cstmSessionHandler->cstm_sess_write(array('developer1'=>'1vinod'));

Case 2: To set two dimensional session data, pass the key and value in array as below
$newData['aaa']['ffff'] = '1111';
$newData['aaa']['bbb'] = '222';
$cstmSessionHandler->cstm_sess_write($newData);

To read session data
Case 1: To read only a single session variable Pass the session key.
$sessionValuesArr = $cstmSessionHandler->cstm_sess_read('aaa');

Case 2: To read all the session which are active. Dont pass any argument
$sessionValuesArr = $cstmSessionHandler->cstm_sess_read();

To unset specific session variable
$cstmSessionHandler->cstm_sess_unset('aaa');

To destroy all the session variable 
$cstmSessionHandler->cstm_sess_destroy();
***********************************************************************************************/

class session
{
    private $link;

    function cstm_sess_open($savePath="", $sessionName)
    {
		
		$servername = 'localhost';
		$username = 'dbuser';
		$password = 'dbpwd';
		$dbname = 'dbname';
		// exit;
        $link = mysqli_connect($servername, $username,$password,$dbname);
        if($link){
            $this->link = $link;
            return true;
        }else{
            return false;
        }
    }

    function cstm_sess_close()
    {
        mysqli_close($this->link);
        return true;
    }

    function cstm_sess_read($sessionDataName="",$id="")
    {
    	
    	if($id == "" || empty($id)){
    		$id = session_id();
    	}
    	// echo '<br/>sess_read';
        $result = mysqli_query($this->link,"SELECT Session_Data FROM my_app_session_table WHERE Session_Id = '".$id."' AND Session_Expires > '".date('Y-m-d H:i:s')."'");

        if($row = mysqli_fetch_assoc($result))
        {
            if(isset($row['Session_Data']) && !empty($row['Session_Data']))
            {
            	$dbReqData = array();
            	$jsonDec = json_decode($row['Session_Data'],true);
            	if (json_last_error() === JSON_ERROR_NONE) {
				    $dbReqData = $jsonDec;	
				}
				if(trim($sessionDataName) == "all" || trim($sessionDataName) == "")
				{
					return $dbReqData;
				}
				else if (array_key_exists($sessionDataName,$dbReqData))
				{
					return $dbReqData[$sessionDataName];
				}
				else
				{
					return "";
				}
            }
            else
			{
				return "";
			}
        }
        else
        {
            return "";
        }
    }

    function cstm_sess_destroy($id="")
    {
    	if($id == "" || empty($id)){
    		$id = session_id();
    	} 
        $result = mysqli_query($this->link,"DELETE FROM my_app_session_table WHERE Session_Id ='".$id."'");
        if($result){
            return true;
        }else{
            return false;
        }
    }

    function cstm_sess_unset($sessionDataName="",$id="")
    {
    	
    	if($id == "" || empty($id)){
    		$id = session_id();
    	}
    	// echo '<br/>sess_read';
        $result = mysqli_query($this->link,"SELECT Session_Data FROM my_app_session_table WHERE Session_Id = '".$id."'");
        if($row = mysqli_fetch_assoc($result))
        {
        	
            if(isset($row['Session_Data']) && !empty($row['Session_Data']))
            {
            	$dbReqData = array();
            	$jsonDec = json_decode($row['Session_Data'],true);
            	if (json_last_error() === JSON_ERROR_NONE) {
				    $dbReqData = $jsonDec;	
				}
				if (array_key_exists($sessionDataName,$dbReqData))
				{
					unset($dbReqData[$sessionDataName]);
					print_r($dbReqData);
					$this->cstm_sess_write($dbReqData,"",true);
					return true;
				}
				else
				{
					return false;
				}
            }
            else
			{
				return false;
			}
        }
        else
        {
            return false;
        }
    }

    function cstm_sess_write($dataArr,$id="",$overwrite=false)
    {

    	if($id == "" || empty($id)){
    		$id = session_id();
    	}
        // echo '<br/>sess_write';
        $DateTime = date('Y-m-d H:i:s');
        $NewDateTime = date('Y-m-d H:i:s',strtotime($DateTime.' + 1 hour'));

        if(!empty($id))
        {	
        	// echo 'not empty';
        	$existingSessDataArr = $this->cstm_sess_read();
        	// print_r($existingSessData);exit;
        	
	        if(is_array($dataArr) && !empty($dataArr) && is_array($existingSessDataArr) && !empty($existingSessDataArr) && $overwrite == false)
	        {
	        	// echo '--1--';
	        	$newSessData = array_merge($existingSessDataArr,$dataArr);
	        }
	        else if(is_array($dataArr) && !empty($dataArr))
	        {
	        	// echo '--2--';
	        	$newSessData = $dataArr;
	        }
	        else
	        {
	        	$newSessData = array();
	        	// echo '--3--';
	        }
	        // print_r($newSessData);

	        $data = json_encode($newSessData);
	        $result = mysqli_query($this->link,"REPLACE INTO my_app_session_table SET Session_Id = '".$id."', Session_Expires = '".$NewDateTime."', Session_Data = '".$data."'");
	        if($result){
	            return true;
	        }else{
	            return false;
	        }
        }
        else
        {
        	return '';
        }
    }
    
    function cstm_sess_gc($maxlifetime="") // $maxlifetime in seconds
    {
    	if(ini_get("session.gc_maxlifetime")){
    		$sessTimeOutVal = ini_get("session.gc_maxlifetime");
    	}
    	else if($maxlifetime!=""){
    		$sessTimeOutVal = $maxlifetime;
    	}
    	else
    	{
    		$sessTimeOutVal = 3600;
    	}
    	$expireQuery = "DELETE FROM my_app_session_table WHERE ((UNIX_TIMESTAMP(Session_Expires) + ".$sessTimeOutVal.") < UNIX_TIMESTAMP())";
        $result = mysqli_query($this->link,$expireQuery);
        if($result){
            return true;
        }else{
            return false;
        }
    }
}
$cstmSessionHandler = new session(); // Initiate the database session object
$cstmSessionHandler->cstm_sess_open(session_id(),"PHPSESSID"); // Open the session similar to session start
$cstmSessionHandler->cstm_sess_gc(); // delete old session exit;
